% Version 1.0
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{jpde-math}[2014/08/16 Example LaTeX class]



\RequirePackage{xspace}
%\usepackage[np]{numprint}
%\usepackage{siunitx}
\RequirePackage{pas-math2}


\RequirePackage[charter]{mathdesign}



\newcommand{\headlinecolor}{\normalcolor}
\LoadClass[french,a4paper,12pt]{article}
\RequirePackage{xcolor}
\definecolor{slcolor}{HTML}{882B21}

\RequirePackage{microtype}

\RequirePackage{multicol}

\RequirePackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}

\PassOptionsToPackage{main=french}{babel}
\RequirePackage{babel}

\DeclareMathAlphabet\mathzapf       {T1}{pzc} {mb} {it}
\DeclareMathAlphabet\mathchorus     {T1}{qzc} {m} {n}
\DeclareMathAlphabet\mathcmsy       {OMS}{cmsy} {m} {n}

\renewcommand{\maketitle}{%
    \noindent\begin{flushright}\Huge\sffamily\bfseries\parskip=0pt%
    \@title
    \tiny\par\noindent\rule{\textwidth}{1.9pt}\end{flushright}
}


\setlength{\parindent}{0em}
\setlength{\parskip}{0.25cm}
